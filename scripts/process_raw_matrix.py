#!/usr/bin/env python3
'''
DOC STUB
'''
import feather
import pandas as pd


def main():

    ###  SNAKEMAKE I/O ###
    raw_matrix    = snakemake.input['raw_data']
    lines_info    = snakemake.input['sample_info']
    where_to_save = snakemake.output['processed_matrix']

    ### SNAKEMAKE PARAMS ###
    round_counts  = bool(snakemake.params['round_counts'])

    # Prepare a dict. to convert DepMap IDs to stripped cell line name
    lines  = pd.read_csv(lines_info,
                         sep=',',
                         usecols=['DepMap_ID', 'stripped_cell_line_name', 'lineage'],
                         index_col=0
                         )
    
    human_readable = lines['stripped_cell_line_name'].to_dict()

    raw_matrix = pd.read_csv(raw_matrix, sep=',', index_col=0)

    # Map DepMap Ids to stripped and drop unmapped entries
    raw_matrix.index = raw_matrix.index.map(human_readable)
    raw_matrix       = raw_matrix[raw_matrix.index.notna()]

    # Transpose matrix for better performance.  Genes as rows now
    raw_matrix = raw_matrix.T

    # Get rid of genes with NaN values (usually ERCC controls or trash annot.)
    raw_matrix = raw_matrix.dropna(axis=0)
    
    # Drop engineered cell lines
    normal_lines = lines.loc[lines['lineage'].str.contains('engineered'), 'stripped_cell_line_name']
    raw_matrix   = raw_matrix.loc[:,~raw_matrix.columns.isin(normal_lines)]

    # Should we round the counts?
    if round_counts:
        raw_matrix  = raw_matrix.round(decimals=0)

    # Get index as columns to avoid R calling it 'X'
    raw_matrix.reset_index(drop=False, inplace=True)
    raw_matrix.rename({'index':'Gene'}, axis=1, inplace=True)

    # CCLE nomenclature is HUGO (ENSEMBL)
    raw_matrix['variance']  = raw_matrix.var(axis=1)
    raw_matrix['Gene'] = [x.split(' ')[0] for x in raw_matrix['Gene'].values.tolist()]

    ## Resolve duplicates keeping most variable genes
    raw_matrix = raw_matrix.sort_values('variance', ascending=False).drop_duplicates('Gene', keep='first').sort_index()	
    raw_matrix.drop('variance', axis=1, inplace=True)
    
    feather.write_dataframe(raw_matrix, where_to_save)



if __name__ == "__main__":
    main()