#!/usr/bin/env python3
'''
DOC STUB
'''
import feather
import pandas as pd

def main():

    ###  SNAKEMAKE I/O ###
    raw_celligner    = snakemake.input['raw_data']
    lines_info       = snakemake.input['sample_info']
    gene_info        = snakemake.input['hgnc_info']
    where_to_save    = snakemake.output['processed_matrix']


    # Prepare a dict. to convert DepMap IDs to stripped cell line name
    lines  = pd.read_csv(lines_info,
                         sep=',',
                         usecols=['DepMap_ID', 'stripped_cell_line_name', 'lineage'],
                         index_col=0)
    
    human_readable = lines['stripped_cell_line_name'].to_dict()

    ## Samples set as index, columns as genes.
    raw_celligner    = pd.read_csv(raw_celligner, sep=',', index_col=0)

    raw_celligner  = raw_celligner.T

    ## Rename cell lines with the stripped cell_line_name. Preserve TCGA
    ## and TARGET sample names
    raw_celligner.rename(human_readable, axis=1, inplace=True)

    # Keeping everything for now
    '''
    ## Subset cell lines for gsva
    is_cell_line = raw_celligner.columns.isin(lines['stripped_cell_line_name'])
    cell_lines   = raw_celligner.loc[:,is_cell_line]

    # Drop engineered cell lines
    normal_lines = lines.loc[lines['lineage'].str.contains('engineered'), 'stripped_cell_line_name']
    cell_lines   = cell_lines.loc[:,~cell_lines.columns.isin(normal_lines)]
    '''

    ## Translate ENSEMBL IDs to HUGO
    gene_info = pd.read_csv(gene_info,
                            sep='\t',
                            usecols=['ensembl_gene_id', 'symbol'])
    
    hugo_dictionary = gene_info.set_index('ensembl_gene_id')['symbol'].to_dict()


    raw_celligner.index = raw_celligner.index.map(hugo_dictionary)

    # Get index as columns to avoid R calling it 'X'
    raw_celligner.reset_index(drop=False, inplace=True)
    raw_celligner.rename({'index':'Gene'}, axis=1, inplace=True)


    feather.write_dataframe(raw_celligner, where_to_save)


if __name__ == "__main__":
    main()