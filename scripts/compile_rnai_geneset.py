import pandas as pd

def main():
    '''
    DOC STUB
    '''

    ### SNAKEMAKE I/O ###
    combined_rnai       = snakemake.input['D2_combined']
    line_info           = snakemake.input['cell_lines']
    where_to_save       = snakemake.output['rnai_gmt']
    rnai_data           = snakemake.output['rnai_data']

    ### SNAKEMAKE PARAMS ###
    depScore     = snakemake.params['dep_score']
    depScore     = float(depScore)

    rnai_lines   = pd.read_csv(combined_rnai, sep=',', index_col=0)

    ccle_lines   = pd.read_csv(line_info,
                                sep=',',
                                usecols=['CCLE_Name',
                                         'stripped_cell_line_name',
                                         'lineage'],
                                )

    
    ##### SHRNA #####
    # Correct scores to match DepMap Top dep.
    means       = rnai_lines.mean(axis=1)
    rnai_lines  = rnai_lines.subtract(means, axis=0)

    # Rename genes
    rnai_lines.index = [x.split(' ')[0] for x in rnai_lines.index.tolist()] 

    # Get cell lines present in omics sample info. Some lines will be missing
    # as not all RNAi lines are from Broad's.
    ccle_name_to_stripped = ccle_lines.set_index('CCLE_Name')['stripped_cell_line_name'].to_dict()
    rnai_lines.columns = rnai_lines.columns.map(ccle_name_to_stripped)
    rnai_lines = rnai_lines.loc[:,rnai_lines.columns[rnai_lines.columns.notna()]]

    # Drop normal (engineered) lines if present
    normal_lines = ccle_lines.loc[ccle_lines['lineage'].str.contains('engineered'), 'stripped_cell_line_name']
    rnai_lines   = rnai_lines.loc[:,~rnai_lines.columns.isin(normal_lines)]

    ## Save the resulting data for further use
    rnai_lines.to_csv(rnai_data, sep=',', index=True, header=True)

    vulnerable_lines = dict()

    for candidate_gene in rnai_lines.index:
        threshold_passed = rnai_lines.loc[candidate_gene] <= depScore
        vulnerable_lines[candidate_gene] = rnai_lines.loc[candidate_gene, threshold_passed].index

    # Convert the dict to a dataframe
    genetic_dependencies = pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in vulnerable_lines.items()]))
    
    # Drop candidates which never passed the threshold
    # Admittedly, this could be done earlier

    has_none = genetic_dependencies.notna().sum() > 0
    has_none = has_none[has_none].index.to_list()

    genetic_dependencies = genetic_dependencies.loc[:,has_none]

    ## Transpose and add a description column to match GMT file format specs.
    genetic_dependencies = genetic_dependencies.T
    genetic_dependencies.insert(0, column='Description', value='Empty')

    genetic_dependencies.to_csv(where_to_save, sep='\t', index=True, header=False, na_rep='')


if __name__ == "__main__":
    main()