import pandas as pd

def main():

    ### SNAKEMAKE I/O ###
    achilles            = snakemake.input['achilles']
    line_info           = snakemake.input['cell_lines']
    where_to_save       = snakemake.output['crispr_gmt']
    crispr_save         = snakemake.output['crispr_data']

    ### SNAKEMAKE PARAMS ###
    p_threshold         = snakemake.params['threshold']

    # cast threshold to float
    p_threshold  = float(p_threshold)

    crispr_lines = pd.read_csv(achilles, sep=',', index_col=0)

    line_info    = pd.read_csv(line_info,
                                sep=',',
                                usecols=['DepMap_ID','stripped_cell_line_name',
                                         'lineage'])


    ## transpose the dataset to gene x line
    crispr_lines = crispr_lines.T

    ## Transform Hugo (entrez id) to Hugo ##
    crispr_lines.index = [x.split(' ')[0] for x in crispr_lines.index]

    ## get stripped names from DepMap Ids
    dep_to_stripped = line_info.set_index('DepMap_ID')['stripped_cell_line_name']
    crispr_lines.columns = crispr_lines.columns.map(dep_to_stripped)

    # Drop engineered (normal) lines, if present
    normal_lines = line_info.loc[line_info['lineage'].str.contains('engineered'), 'stripped_cell_line_name']
    crispr_lines = crispr_lines.loc[:,~crispr_lines.columns.isin(normal_lines)]

    ## Save the resulting dataset for further use
    crispr_lines.to_csv(crispr_save, sep=',', index=True, header=True)

    ## Get, for each gene, cell lines with p > p_threshold of being affected if 
    # the gene is knocked
    vulnerable_lines = dict()

    for candidate_gene in crispr_lines.index:
        threshold_passed = crispr_lines.loc[candidate_gene] >= p_threshold
        vulnerable_lines[candidate_gene] = crispr_lines.loc[candidate_gene, threshold_passed].index

    # Convert the dict to a dataframe
    genetic_dependencies = pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in vulnerable_lines.items()]))
    
    # Drop candidates which never passed the threshold
    # Admittedly, this could be done earlier

    has_none = genetic_dependencies.notna().sum() > 0
    has_none = has_none[has_none].index.to_list()

    genetic_dependencies = genetic_dependencies.loc[:,has_none]

    ## Transpose and add a description column to match GMT file format specs.
    genetic_dependencies = genetic_dependencies.T
    genetic_dependencies.insert(0, column='Description', value='Empty')

    genetic_dependencies.to_csv(where_to_save, sep='\t', index=True, header=False, na_rep='')




if __name__ == "__main__":
    main()