import streamlit as st
import pandas as pd

## import pages ##

import data_explorer
import sing_explorer

## --- GLOBAL CFG --- ##
st.set_page_config(page_title='Dependency explorer',
                   page_icon=None,
                   layout='centered',
                   initial_sidebar_state='auto')

st.title('Dependency explorer')


## Define available pages
pages = {'Data_explorer': data_explorer,
         'Enrichment explorer': sing_explorer}


## --- SIDEBAR CONTROLLERS --- ##
st.sidebar.header('Navigation')

selected_page = st.sidebar.radio('Go to:', list(pages.keys()))


## --- PAGE CONTROLLER --- ##
page = pages[selected_page]
page.app()