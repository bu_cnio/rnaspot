import streamlit as st
import pandas as pd

def app():

    ## --- CACHING FUNCS --- ##
    available_signatures = st.cache(pd.read_csv)('signatures.csv', sep=',')


    @st.cache(allow_output_mutation=True) # Making pathways more readable
    def load_dependencies(chosen_dataset):

        dataset = pd.read_csv(chosen_dataset, sep=',',
        usecols=['pathway','dependency','padj','NES', 'size', 'is_member', 'leadingEdge'])
        
        dataset = dataset[['pathway', 'dependency', 'NES',
                       'padj', 'is_member', 'size', 'leadingEdge']]

        return dataset
    

    ## APP HEADER ##
    st.header('Dataframe explorer')

    ## Controllers ##
    type_dependency = st.sidebar.selectbox(label='Choose dependency source', 
                                           options=['CRISPR', 'RNAI'], index=0)


    filter_mode = st.sidebar.radio(label='Choose filtering mode', 
                               options=['Pathways', 'Dependency'])

    option      = st.sidebar.selectbox('Select MSIGDB signature',
                                    available_signatures['name'].unique(),
                                    index=0)

    gene_query = st.sidebar.text_input('Input your gene query as HUGO symbols separated with comma') 

    chosen_signature  = f'/local/sagarcia/rnaspot/results/pathways_dependencies_{option}_{type_dependency.lower()}.csv'

    full_name              = available_signatures.loc[available_signatures['name'] == option, 'signature'].values
    full_name              = full_name[0]
    chosen_signature_file = f'/local/sagarcia/rnaspot/datasets/signatures/{full_name}'

    signature_dependencies = load_dependencies(chosen_signature)

    ## gene query ##
    gene_query = gene_query.split(',')

    ## get pathways where these genes are present
    queried_gene_pathways = list()

    with open(chosen_signature_file, 'r') as signature:

        lines = signature.readlines()
        for gene in gene_query:
            for line in lines:
                if gene in line:
                    queried_gene_pathways.append(line.split('\t')[0])
    

# Hacky, but streamlit textbox field work this way...
    if gene_query[0] != '':
        signature_dependencies = signature_dependencies[signature_dependencies['pathway'].isin(queried_gene_pathways)]


    match_column = 'pathway'

    if filter_mode == 'Pathways':
        available_criterias = signature_dependencies['pathway'].sort_values().unique()
        match_column = 'pathway'
    else:
        available_criterias = signature_dependencies['dependency'].sort_values().unique()
        match_column = 'dependency'

    criteria  = st.sidebar.multiselect(f'Filter by {filter_mode}', available_criterias, default=[])
    threshold = st.sidebar.slider('FDR threshold', min_value=0.001, max_value=1.0, value=1.0)

    if not criteria:
        st.error("Please select at least one item from genetic dependency or pathway.")
    

# Filter signature
    filtered_signatures = signature_dependencies[(signature_dependencies[match_column].isin(criteria)) & (signature_dependencies['padj'] <= threshold)]

    st.dataframe(filtered_signatures[['pathway', 'dependency', 'NES', 'padj', 'is_member', 'size']])
    
    st.write('Number of tests performed for this dataset:', len(signature_dependencies.index))
    st.write('Matching results at chosen FDR threshold:', len(filtered_signatures.index))
    st.write('Unique pathways at chosen FDR threshold:', filtered_signatures['pathway'].nunique())
    st.write('Unique dependencies at chosen FDR threshold:', filtered_signatures['dependency'].nunique())

    st.header('Leading Edge Analysis')
    st.dataframe(filtered_signatures[['dependency', 'leadingEdge']])

