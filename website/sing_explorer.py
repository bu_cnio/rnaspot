import streamlit as st
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
import umap
import umap.plot


def app():

    ## --- CACHING FUNCS --- ##
    available_signatures = st.cache(pd.read_csv)('signatures.csv', sep=',')


    @st.cache(allow_output_mutation=False) # Making pathways more readable
    def load_matrix(chosen_dataset):

        dataset = pd.read_feather(chosen_dataset)
        dataset.set_index('pathway', inplace=True)

        return dataset
    

    ## APP HEADER ##
    st.header('Enrichment explorer')

    ## --- SIDEBAR CONTROLLERS --- ##

    signature = st.sidebar.selectbox(label='Choose MSigDB collection:',
                                     options=available_signatures['name'].unique(),
                                     index=0)

    # We have to load it here for the next controller to work
    sing_matrix = load_matrix(f'/local/sagarcia/rnaspot/results/single_sample_{signature}.feather')
    
    mapper = umap.UMAP().fit(sing_matrix)

    available_signatures = sing_matrix.columns

    pathway_to_draw = st.sidebar.selectbox(label='Select pathway to draw:',
                                           options=available_signatures,
                                           index=0)



    ## plot UMAP ##
    fig, ax = plt.subplots()

    a = umap.plot.points(mapper,
                         background='white',
                         values=sing_matrix.loc[:,pathway_to_draw],
                         cmap='Reds',
                         show_legend=False)

    fig = a.get_figure()
    st.pyplot(fig, clear_figure=True)