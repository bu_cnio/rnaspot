import sys

import glob
import pandas as pd


from snakemake.utils import min_version

#### GLOBAL PARAMETERS AND CONFIG ####
min_version('5.3.0')

configfile: 'config.yaml'

#### DEFINE I/O DIRECTORIES ####
INDIR      = config['datasets']
OUTDIR     = config['results']
LOGDIR     = config['logdir']
SIG_TABLE  = pd.read_csv('signatures.csv', sep=',')
SIGNATURES = SIG_TABLE['name']

#### GLOBAL scope functions ####

def get_resource(rule,resource) -> int:
	'''
	Attempt to parse config.yaml to retrieve resources available for a given
	rule. It will revert to default if a key error is found. Returns an int.
	with the allocated resources available for said rule. Ex: "threads": 1
	'''

	try:
		return config['rules'][rule]['res'][resource]
	except KeyError: # TODO: LOG THIS
		print(f'Failed to resolve resource for {rule}/{resource}: using default parameters')
		return config["rules"]['default']['res'][resource]

#TODO: add defaults
def get_params(rule,param) -> int:
	'''
	Attempt to parse config.yaml to retrieve parameters available for a given
	rule. It will crash otherwise.
	'''
	try:
		return config['rules'][rule]['params'][param]
	except KeyError: # TODO: LOG THIS
		print(f'Failed to resolve parameter for {rule}/{param}: Exiting...')
		sys.exit(1)


def get_signature_list() -> list:
	return SIG_TABLE['signature'].to_list()


# TODO: ORPHAN FUNC?
def get_full_signature_name(wildcards):
	'''	
	MSIGDB names can be quite verbose. This function takes short names from
	signatures.csv and returns the full filename for those functions requiring
	it.
	'''
	short_name = wildcards.signature
	full_name  = SIG_TABLE[SIG_TABLE['name'] == short_name]['signature'].values[0]
	full_path  =  f'{INDIR}/signatures/{full_name}'
	return full_path

# TODO: ORPHAN FUNC?
def get_signature_path(signature_name:str):
	'''	
	MSIGDB names can be quite verbose. This function takes short names from
	signatures.csv and returns the full filename for those functions requiring
	it.
	'''
	short_name = signature_name
	full_name  = SIG_TABLE[SIG_TABLE['name'] == short_name]['signature'].values[0]
	full_path  =  f'{INDIR}/signatures/{full_name}'
	return full_path



rule all:
	input:
		expand(OUTDIR + '/pathways_dependencies_{signature}_crispr.csv', signature=SIGNATURES),
		expand(OUTDIR + '/pathways_dependencies_{signature}_rnai.csv', signature=SIGNATURES)



rule build_crispr_geneset:
	input:
		achilles=f'{INDIR}/Achilles_gene_dependency.csv',
		cell_lines=f'{INDIR}/sample_info.csv'
	output:
		crispr_gmt=f'{OUTDIR}/crispr_dependencies.gmt',
		crispr_data = f'{OUTDIR}/crispr_data.csv'
	params:
		threshold=get_params('build_crispr_geneset', 'essential_probability')
	threads:
		get_resource('build_crispr_geneset', 'threads')
	resources:
		mem_mb=get_resource('build_crispr_geneset', 'mem_mb'),
		walltime=get_resource('build_crispr_geneset', 'walltime')
	benchmark:
		'benchmarks/build_crispr_geneset.txt'
	conda:
		'envs/matrix_manipulation.yaml'
	script:
		'./scripts/compile_crispr_geneset.py'


rule build_rnai_geneset:
	input:
		D2_combined=f'{INDIR}/D2_combined_gene_dep_scores.csv',
		cell_lines=f'{INDIR}/sample_info.csv'
	output:
		rnai_gmt=f'{OUTDIR}/rnai_dependencies.gmt',
		rnai_data=f'{OUTDIR}/rnai_data.csv'
	params:
		dep_score=get_params('build_rnai_geneset', 'min_dependency_score')
	threads:
		get_resource('build_rnai_geneset', 'threads')
	resources:
		mem_mb=get_resource('build_rnai_geneset', 'mem_mb'),
		walltime=get_resource('build_rnai_geneset', 'walltime')
	benchmark:
		'benchmarks/build_rnai_geneset.txt'
	conda:
		'envs/matrix_manipulation.yaml'
	script:
		'./scripts/compile_rnai_geneset.py'


rule combine_genesets:
	input:
		raw_gmt_files = expand(INDIR + '/signatures/{individual_gmt}', individual_gmt=get_signature_list())
	output:
		combined_geneset = f'{OUTDIR}/combined_geneset.gmt',
	threads:
		get_resource('combine_genesets', 'threads')
	resources:
		mem_mb=get_resource('combine_genesets', 'mem_mb'),
		walltime=get_resource('combine_genesets', 'walltime')
	benchmark:
		'benchmarks/combine_genesets.txt'
	shell:
		'cat {input.raw_gmt_files} > {output.combined_geneset}'

#TODO: ORPHAN RULE
rule prepare_expression_matrix:
	input:
		raw_data=f'{INDIR}/CCLE_RNAseq_reads.csv',
		sample_info=f'{INDIR}/sample_info.csv'
	output:
		processed_matrix=f'{OUTDIR}/expression_matrix.feather'
	params:
		round_counts=get_params('prepare_expression_matrix', 'round_counts')
	threads:
		get_resource('prepare_expression_matrix', 'threads')
	resources:
		mem_mb=get_resource('prepare_expression_matrix', 'mem_mb'),
		walltime=get_resource('prepare_expression_matrix', 'walltime')
	benchmark: 
		'benchmarks/prepare_expr_matrix.txt'
	conda:
		'envs/matrix_manipulation.yaml'
	script:
		"./scripts/process_raw_matrix.py"


rule prepare_celligner_data:
	input:
		raw_celligner=f'{INDIR}/Celligner_aligned_data.csv',
		sample_info=f'{INDIR}/sample_info.csv',
		hgnc_info = f'{INDIR}/hgnc_complete_set_7.24.2018.txt'
	output:
		processed_matrix=f'{OUTDIR}/complete_celligner_matrix.feather'
	threads:
		get_resource('prepare_celligner_data', 'threads')
	resources:
		mem_mb=get_resource('prepare_celligner_data', 'mem_mb'),
		walltime=get_resource('prepare_celligner_data', 'walltime')
	benchmark: 
		'benchmarks/prepare_celligner_data.txt'
	conda:
		'envs/matrix_manipulation.yaml'
	script:
		"./scripts/process_raw_celligner_data.py"

##TODO: ORPHAN RULE
rule normalize_matrix:
	input:
		raw_data = rules.prepare_expression_matrix.output.processed_matrix
	output:
		normalized_matrix = f'{OUTDIR}/normalized_matrix.feather'
	threads:
		get_resource('normalize_matrix', 'threads')
	resources:
		mem_mb=get_resource('normalize_matrix', 'mem_mb'),
		walltime=get_resource('normalize_matrix', 'walltime')
	conda:
		'envs/matrix_normalization.yaml'
	script:
		"./scripts/normalize_matrix.R"


rule gsva:
	input:
		expression_matrix= rules.prepare_celligner_data.output.processed_matrix,
		signature= combine_genesets.output
	output:
		gsva_matrix=f'{OUTDIR}/single_sample_celligner.feather'
	params:
		min_geneset=get_params('gsva', 'min_gset_size'),
		max_geneset=get_params('gsva', 'max_gset_size')
	threads:
		get_resource('gsva', 'threads')
	resources:
		mem_mb=get_resource('gsva', 'mem_mb'),
		walltime=get_resource('gsva', 'walltime')
	benchmark:
		'benchmarks/gsva.{signature}.txt'
	conda:
		'envs/gsva.yaml'
	script:
		'./scripts/gsva.R'	


rule fgsea_crispr:
	input:
		gsva_matrix=rules.gsva.output.gsva_matrix,
		dependencies=rules.build_crispr_geneset.output.crispr_gmt,
		signature   =get_full_signature_name
	output:
		pathways_dependencies= OUTDIR + '/pathways_dependencies_{signature}_crispr.csv'
	threads:
		get_resource('fgsea_crispr', 'threads')
	resources:
		mem_mb=get_resource('fgsea_crispr',  'mem_mb'),
		walltime=get_resource('fgsea_crispr', 'walltime')
	params:
		min_geneset_size=get_params('fgsea_crispr', 'min_gset_size'),
		max_geneset_size=get_params('fgsea_crispr', 'max_gset_size'),
		simple_permutations=get_params('fgsea_crispr', 'nperm')
	benchmark: 
		'benchmarks/{signature}.fgsea_crispr.txt'
	conda:
		"./envs/fgsea.yaml"
	script:
		"./scripts/prerranked_gsea.R"


rule fgsea_rnai:
	input:
		gsva_matrix=rules.gsva.output.gsva_matrix,
		dependencies=rules.build_rnai_geneset.output.rnai_gmt,
		signature   =get_full_signature_name
	output:
		pathways_dependencies= OUTDIR + '/pathways_dependencies_{signature}_rnai.csv'
	threads:
		get_resource('fgsea_rnai', 'threads')
	resources:
		mem_mb=get_resource('fgsea_rnai',  'mem_mb'),
		walltime=get_resource('fgsea_rnai', 'walltime')
	params:
		min_geneset_size=get_params('fgsea_rnai', 'min_gset_size'),
		max_geneset_size=get_params('fgsea_rnai', 'max_gset_size'),
		simple_permutations=get_params('fgsea_crispr', 'nperm')
	benchmark: 
		'benchmarks/{signature}.fgsea_rnai.txt'
	conda:
		"./envs/fgsea.yaml"
	script:
		"./scripts/prerranked_gsea.R"